o "Deploying app.jar to docker folder"
packageName=`ls target/common-config-server*.jar`
versionid=`echo $packageName | awk -F "-" '{ print $2}'`
versionname=`echo $packageName | awk -F "-" '{ print $3}' | awk -F "." '{ print $1}'`
version=`echo $versionid-$versionname`
echo "version: $version"
cp -r $packageName deployment/app.jar
dockerImageName=mdgb1311/config-server
dockerRedName=employee
dockerContainerName=config-server-mdgb
dockerpid=`docker ps -a | grep $dockerImageName | grep "Up" | awk -F " " '{ print $1 }'`
if [[ $dockerpid != "" ]];then 
   docker kill $dockerpid
   docker rm $dockerpid
fi
docker build -t $dockerImageName deployment/.
docker run -d --name=$dockerContainerName --network=$dockerRedName -p 8090:8090 $dockerImageName 
dockerImageId=`docker images | grep $dockerImageName | grep latest | awk -F " " '{print $3}'` 
docker tag $dockerImageId $dockerImageName:$version
docker login -u mdgb1311 -p Garcia1311
docker push $dockerImageName:$version
